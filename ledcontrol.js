var http = require('http').createServer(); //require http server, and create server with function handler()
var io = require('socket.io')(http) //require socket.io module and pass the http object (server)
var Gpio = require('pigpio').Gpio; //include pigpio to interact with the GPIO
var ks = require('node-key-sender');

// Variables to keep track of pins and intervals
let currentPins = new Set();
let currentPinsSecondLayer = new Set();
let currentIntervals = new Set();

// Variables for pulsing LED
let brightness = 0;
let goingUp = true;

const leftButton = new Gpio(2, {
  mode: Gpio.INPUT,
  pullUpDown: Gpio.PUD_UP,
  alert: true
});

const rightButton = new Gpio(3, {
  mode: Gpio.INPUT,
  pullUpDown: Gpio.PUD_UP,
  alert: true
});

const stopButton = new Gpio(18, {
  mode: Gpio.INPUT,
  pullUpDown: Gpio.PUD_UP,
  alert: true
});

const playButton = new Gpio(15, {
  mode: Gpio.INPUT,
  pullUpDown: Gpio.PUD_UP,
  alert: true
});

leftButton.glitchFilter(20000);
rightButton.glitchFilter(20000);
stopButton.glitchFilter(20000);
playButton.glitchFilter(20000);

let target1 = new Gpio(5, { mode: Gpio.OUTPUT });
    target1.digitalWrite(0); // Turn LED off

let target2 = new Gpio(6, { mode: Gpio.OUTPUT });
    target2.digitalWrite(0); // Turn LED off

let target3 = new Gpio(7, { mode: Gpio.OUTPUT });
    target3.digitalWrite(0); // Turn LED off

http.listen(8080); //listen to port 8080

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function selfTest() {
  let twelveOClock = new Gpio(9, { mode: Gpio.OUTPUT });
  let oneOClock = new Gpio(11, { mode: Gpio.OUTPUT });
  let twoOClock = new Gpio(5, { mode: Gpio.OUTPUT });
  let threeOClock = new Gpio(6, { mode: Gpio.OUTPUT });
  let fourOClock = new Gpio(13, { mode: Gpio.OUTPUT });
  let fiveOClock = new Gpio(19, { mode: Gpio.OUTPUT });
  let sixOClock = new Gpio(26, { mode: Gpio.OUTPUT });
  let sevenOClock = new Gpio(7, { mode: Gpio.OUTPUT });
  let eightOClock = new Gpio(12, { mode: Gpio.OUTPUT });
  let nineOClock = new Gpio(16, { mode: Gpio.OUTPUT });
  let tenOClock = new Gpio(20, { mode: Gpio.OUTPUT });
  let elevenOClock = new Gpio(21, { mode: Gpio.OUTPUT });
  let rightButton = new Gpio(10, { mode: Gpio.OUTPUT });
  let playButton = new Gpio(22, { mode: Gpio.OUTPUT });
  let stopButton = new Gpio(17, { mode: Gpio.OUTPUT });
  let leftButton = new Gpio(27, { mode: Gpio.OUTPUT });

  while(true) {

  writePin(5, twelveOClock);
  await sleep(250);
  twelveOClock.digitalWrite(0);

  writePin(5, oneOClock);
  await sleep(250);
  oneOClock.digitalWrite(0);

  writePin(5, twoOClock);
  await sleep(250);
  twoOClock.digitalWrite(0);

  writePin(5, threeOClock);
  await sleep(250);
  threeOClock.digitalWrite(0);

  writePin(5, fourOClock);
  await sleep(250);
  fourOClock.digitalWrite(0);

  writePin(5, fiveOClock);
  await sleep(250);
  fiveOClock.digitalWrite(0);

  writePin(5, sixOClock);
  await sleep(250);
  sixOClock.digitalWrite(0);

  writePin(5, sevenOClock);
  await sleep(250);
  sevenOClock.digitalWrite(0);

  writePin(5, eightOClock);
  await sleep(250);
  eightOClock.digitalWrite(0);

  writePin(5, nineOClock);
  await sleep(250);
  nineOClock.digitalWrite(0);

  writePin(5, tenOClock);
  await sleep(250);
  tenOClock.digitalWrite(0);

  writePin(5, elevenOClock);
  await sleep(250);
  elevenOClock.digitalWrite(0);

  writePin(255, rightButton);
  await sleep(250);
  rightButton.digitalWrite(0);

  writePin(255, playButton);
  await sleep(250);
  playButton.digitalWrite(0);

  writePin(255, stopButton);
  await sleep(250);
  stopButton.digitalWrite(0);

  writePin(255, leftButton);
  await sleep(250);
  leftButton.digitalWrite(0);
  
  }
}

//selfTest();

console.log("GPIO webservice started!")

io.sockets.on('connection', function (socket) { // Web Socket Connection
  /* Button Event Emissions */
  leftButton.on('alert', (level, tick) => {
    if (level === 0) {
      console.log("Left button pressed");
      socket.emit('button', 'left');
    }
  });

  rightButton.on('alert', (level, tick) => {
    if (level === 0) {
      console.log("Right button pressed");
      socket.emit('button', 'right');
    }
  });

  stopButton.on('alert', (level, tick) => {
    if (level === 0) {
      console.log("Stop button pressed");
      socket.emit('button', 'stop');
    }
  });

  playButton.on('alert', (level, tick) => {
    if (level === 0) {
      console.log("Play button pressed");
      socket.emit('button', 'play');
    }
  });

  /* LED Control Event Inputs */
  socket.on('setOutput', function (data) { // Set a GPIO pin to constant with data object (target, value)
    console.log("Setting Pin w/ Constant Output")
    console.log(data);
    let target = new Gpio(data.target, { mode: Gpio.OUTPUT });
    addToCurrentPins(data, false);
    writePin(data.value, target);
    console.log(""); // Empty line to make a prettier log
  });

  socket.on('clearAllOutputs', function (data) {
    clearAllIntervals();

    currentPins.forEach(pin => {
      let target = new Gpio(pin, { mode: Gpio.OUTPUT });
      target.digitalWrite(0); // Turn LED off
    });
  });

  socket.on('clearAllOutputsSecondLayer', function (data) {
    clearAllIntervals();

    currentPinsSecondLayer.forEach(pin => {
      let target = new Gpio(pin, { mode: Gpio.OUTPUT });
      target.digitalWrite(0); // Turn LED off
    });
  });

  socket.on('setOutputArray', function (data) { // Set a GPIO pin to constant with data object (target, value)
    console.log("Setting Pin w/ Constant Output")
    data.forEach(dataObj => {
      console.log(dataObj);
      let target = new Gpio(dataObj.target, { mode: Gpio.OUTPUT });
      addToCurrentPins(dataObj, false);
      writePin(dataObj.value, target);
      console.log(""); // Empty line to make a prettier log
    });
  });

  socket.on('setOutputArraySecondLayer', function (data) { // Set a GPIO pin to constant with data object (target, value)
    console.log("Setting Pin w/ Constant Output")
    data.forEach(dataObj => {
      console.log(dataObj);
      let target = new Gpio(dataObj.target, { mode: Gpio.OUTPUT });
      addToCurrentPins(dataObj, true);
      writePin(dataObj.value, target);
      console.log(""); // Empty line to make a prettier log
    });
  });

  socket.on('setPulse', function (data) { // Set a GPIO pin to pulse with data object
    clearAllIntervals();
    console.log("Setting Pin w/ Pulsing Output")
    console.log(data);
    let target = new Gpio(data.target, { mode: Gpio.OUTPUT });
    addToCurrentPins(data, false);
    let interval = setInterval(() => {
      if (goingUp) {
        brightness += 1;
      } else {
        brightness -= 1;
      }
      writePin(brightness, target);
      if (brightness == data.value) {
        goingUp = false;
      } else if (brightness == 0) {
        goingUp = true;
      }
    }, data.speed);
    addToCurrentIntervals(interval);
    console.log(""); // Empty line to make a prettier log
  });

  socket.on('setPulseArray', function (data) { // Set a GPIO pin to pulse with data object
    clearAllIntervals();
    console.log("Setting Pin w/ Pulsing Output")
    data.forEach(dataObj => {
      console.log(dataObj);
      addToCurrentPins(dataObj, false);
    });

    let interval = setInterval(() => {
      if (goingUp) {
        brightness += 1;
      } else {
        brightness -= 1;
      }

      data.forEach(dataObj => {
        let target = new Gpio(dataObj.target, { mode: Gpio.OUTPUT });
        writePin(brightness, target);
      });

      if (brightness == data[0].value) {
        goingUp = false;
      } else if (brightness == 0) {
        goingUp = true;
      }
    }, data[0].speed);
    addToCurrentIntervals(interval);
    console.log(""); // Empty line to make a prettier log
  });
});

process.on('SIGINT', function () { //on ctrl+c
  clearAllIntervals();

  currentPins.forEach(pin => {
    let target = new Gpio(pin, { mode: Gpio.OUTPUT });
    target.digitalWrite(0); // Turn LED off
  });

  process.exit(); //exit completely
});

function addToCurrentPins(request, isSecondLayer) {
  if(isSecondLayer) {
    currentPinsSecondLayer.add(request.target);
  } else {
    currentPins.add(request.target);
  }
  
}

function addToCurrentIntervals(request) {
  currentIntervals.add(request)
}

function clearAllIntervals() {
  if (currentIntervals.size > 0) {
    for (var interval of currentIntervals) {
      currentIntervals.delete(interval);
      clearInterval(interval);
    }
  }
}

function writePin(valueToSet, target) {
  value = parseInt(valueToSet);
  target.pwmWrite(value);
}